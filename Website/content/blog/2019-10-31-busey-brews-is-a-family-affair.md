---
title: "Busey Brews is a family affair"
date: 2019-10-31
tags: Article
author: Stephanie Andelman
image: "blog/2019-10-31-busey-brews-is-a-family-affair.jpg"
---

The below article appeared in the October issue of [The Mountain Ear](https://themtnear.com).

---

What's it like to own a family-run business in Nederland? Ask Kyle Busey who bought the once Wild Mountain then James Peak Smokehouse and Brewery. Rebranded as Busey Brews, Kyle's brother is head brewer, and their parents are key strategists. You'll find all of them greeting you at the door, taking your orders, pouring your drinks, and asking for your feedback.

Kyle came to visit brother Nick (aka DJ Gangsterish) in Jamestown 12 years ago. On his way back east, he decided that Colorado would be his new home. In 2007, the brothers moved to Nederland. Parents Buzz and Kim followed them out here and moved to Bar K Ranch a few years ago.

When Buzz took the family around on his business to resorts, clubs, hotels, Kyle became hooked on hospitality. He started serving at age 14 and has worked in every restaurant position possible. When he earned his Hospitality Food & Beverage Degree from James Madison College, he worked in the high-paced hotel corporate culture. Ten years of that experience led him to yearn for community and local establishments, so after moving to CO, he bartended and served at The Kitchen and First Street Pub & Grill - and managed food and beverage at a local music venue and Eldora's Lookout Lodge. Being drawn to customer service and innovation, Kyle also helped expand Nederland's Grow in Peace store.

When Kyle became the General Manager at James Peak this past April, he was soon asked to consider purchasing the business and building. The conversations got serious this summer, so he went to work on financing the deal with advice from his parents. At the end of August, the Buseys were fully ingrained. Kyle was the new owner of James Peak, and Nick came on board to replace, upgrade, and add brewing equipment. The Buseys and friends renovated the interior and opened as Busey Brews on October 19. Buzz was in charge of the woodwork now displayed on their sign and tap handles. Kim oversaw the lighting, colors, and deep cleaning efforts. All of the Buseys helped design and purchase the merchandise, including their special glassware and taster trays (made of mining sieves).

Of their 16 taps, five are currently Busey Brew collaboration beers. Nick is brewing unique flavors and plans to produce enough beer for all 16 taps in 2020. On October 22, he received notice that their TTB license was approved, so the beers can now be sold under the Busey Brews brand. When Nick's wife bought him a home brew kit for Christmas, he became obsessed with brewing. In one year, he learned the craft and was able to produce four kegs for his wedding. Now his wife works for the Brewers Association, and Nick spends half of his week brewing in Nederland. Judd Kaufman, of Gold Dirt Distillery, has been an invaluable resource to Busey's brewery, sharing his brewing knowledge and helping them clean equipment for quality production.

Busey Brews is an open source brewery, so every beer's timeline, process, and ingredients history can be found on their website. When they start to sell crowler cans up to 32 ounces, each can's QR code will lead you to this historical information, as well.

Nick is working on a barrel program, so he can brew beers with aged oak barrels. He's also exploring sours inoculated with micro flora. The popularity of beer blends will lead to Busey Brews Mixers, where the bartender will pour two beers into one pint glass. For the non alcohol drinkers, Nick keeps two of his homemade kombuchas on tap and has plans to brew birch beer and other carbonated options. After winter, plexiglass will be installed by the bar so patrons can look down into the brewing operations where four tanks and two fermenters are going full tilt.

Patrons will also be able to view the "Fermentation Station" upstairs, where brewing, hot sauce, meats, and toppings will be fermented alongside hydroponically grown herbs. In the next few months, your entire corned beef reuben will been treated, seasoned, and smoked onsite.

When Kyle thinks about the 22 employees he manages, he exclaims how the staff and customers energize him. His drive for top notch consistency, quality, and customer experience makes his long weeks worth every minute. When he's able to expand his kitchen and close in the deck for colder months, he will be doing his part to serve the community and its visitors. On giving back, Kyle is inviting local non profits to have fundraisers there, so he can donate a significant percentage of sales to their causes.

Kyle may have put his musician ambitions to the side, as he's a talented singer, songwriter, guitar player, and saxophonist, but he brings live music and trivia every week to Busey Brews. During ski season, he will offer discounts to Eldora employees and ski pass holders. Happy Hours (Monday through Friday, 3 to 6 p.m.) include Busey beers and freshly smoked protein. Even vegetarians can be well fed, with a smile from the family of servers embracing Nederland and its needs for quality experiences.