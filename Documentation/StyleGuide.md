# Style Guide

## Colors

### Busey Blue

Our main accent color is `Busey Blue`. It's a light blue.

Hex Code: #0188C0

### Other Colors

The main text in the logo should be black or white depending on the shade of the background.

The mountains and the reverse B should be Busey Blue.

If the background is Busey Blue, the text should be white and mountains black.

When using slate gray as a background, use #363636

## Logo

The `full logo` is the logo including the mountains and the `Busey Brews` text. It should be used in most cases.

The `double b` is just the two Bs in the center of the logo. It should be used when it makes sense for stylistic and sizing purposes.