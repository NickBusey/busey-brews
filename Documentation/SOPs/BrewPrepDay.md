# Prep Day

## Spent Grain Bins

Ensure there are empty, clean spent grain bins ready to go.

## Grain

Bring grain bags that will be used on brew day inside to warm up over night.

## Clean

Make sure all the vessels in the brew system are clean.

## Water

Fill the HLT up with water, and fill the Kettle up to the weld of the top ladder rung.

Set Kettle temp to 184 F. Set HLT temp to 200.

Have the last person to leave at night turn them on so they are hot first thing in the morning.
