# Transfer Tee

**Last Updated: 2019-11-09 by Nick Busey**

## Object and Scope

A transfer tee is used to make transferring beer and chems easier.

## Equipment Needed

1. 3 x Butterfly Valves
1. Sight Glass
1. Tri-clamp Tee Connector
1. 4 x Tri-clamps 

## Assembly

**Insert picture here with sides of T labeled.**

Attach the sight glass to side A with a tri-clamp.
Attach butterfly valves to the sight glass and the remaining 2 sides of the T.

## Usage

1. Close all 3 butterfly valves
1. Attach side C of the T to the port on the tank you are filling from, typically the dump port.
1. Attach side A to the hose with the incoming product. This hose should be full of sani.
1. Turn on your pump, and open the butterfly valve on side A and C to allow the sani to flow into the tank.
    1. This is [Position 1](#position-1)
1. As soon as you see beer in the sight glass, close butterfly valve C.
    1. This is [Position 2](#position-2)
1. Throttle butterlfy valve A to slow the transfer.
1. Open butterfly valve B to dump the sani/beer mix.
    1. Wait for the fluid being dumped to turn to 100% beer.
    1. This is [Position 3](#position-3)
1. Turn off butterfly valve B.
1. Turn off butterfly valve A.
1. Open butterfly valve C.
1. Open butterfly valve B and let the sani in the tank drain out.
1. Once drained, close valve B.
1. Open valve A. You now have pure beer flowing into a sanitized port and tank.

## Graphs

### Position 1

Draining sani from hose into tank

<div class="mermaid">
graph LR
    subgraph "Sight Glass"
      AA[Sani]
    end
    subgraph "Butterfly Valve A "
        AA --> A[Open]
    end
    subgraph Tee
        A --> B[Sani]
    end
    subgraph "Butterfly Valve C"
        B --> C[Open]
    end
    subgraph "Tank"
        C --> D[Sani]
    end
    subgraph "Butterfly Valve B"
      B --> E[Closed]
    end
    subgraph "Drain"
      E --> F[Dry]
    end
</div>

### Position 2

Stopping sani transfer

<div class="mermaid">
graph LR
    subgraph "Sight Glass"
      AA[Sani/Beer]
    end
    subgraph "Butterfly Valve A "
        AA --> A[Open]
    end
    subgraph Tee
        A --> B[Sani/Beer]
    end
    subgraph "Butterfly Valve C"
        B --> C[Closed]
    end
    subgraph "Tank"
        C --> D[Sani]
    end
    subgraph "Butterfly Valve B"
      B --> E[Closed]
    end
    subgraph "Drain"
      E --> F[Dry]
    end
</div>

### Position 3

Dumping sani/beer mix

<div class="mermaid">
graph LR
    subgraph "Sight Glass"
      AA[Sani/Beer]
    end
    subgraph "Butterfly Valve A "
        AA --> A[Open]
    end
    subgraph Tee
        A --> B[Sani/Beer]
    end
    subgraph "Butterfly Valve C"
        B --> C[Closed]
    end
    subgraph "Tank"
        C --> D[Sani]
    end
    subgraph "Butterfly Valve B"
      B --> E[Open]
    end
    subgraph "Drain"
      E --> F[Sani/Beer]
    end
</div>

### Position 4

Stopping dump

<div class="mermaid">
graph LR
    subgraph "Sight Glass"
      AA[Beer]
    end
    subgraph "Butterfly Valve A "
        AA --> A[Open]
    end
    subgraph Tee
        A --> B[Beer]
    end
    subgraph "Butterfly Valve C"
        B --> C[Closed]
    end
    subgraph "Tank"
        C --> D[Sani]
    end
    subgraph "Butterfly Valve B"
      B --> E[Closed]
    end
    subgraph "Drain"
      E --> F[Dry]
    end
</div>

### Position 5

Dumping sani from tank

<div class="mermaid">
graph LR
    subgraph "Sight Glass"
      AA[Sani]
    end
    subgraph "Butterfly Valve A "
        AA --> A[Open]
    end
    subgraph Tee
        A --> B[Sani]
    end
    subgraph "Butterfly Valve C"
        B --> C[Open]
    end
    subgraph "Tank"
        C --> D[Sani]
    end
    subgraph "Butterfly Valve B"
      B --> E[Open]
    end
    subgraph "Drain"
      E --> F[Sani]
    end
</div>

### Position 6

Stopping Dump

<div class="mermaid">
graph LR
    subgraph "Sight Glass"
      AA[Sani]
    end
    subgraph "Butterfly Valve A "
        AA --> A[Open]
    end
    subgraph Tee
        A --> B[Sani]
    end
    subgraph "Butterfly Valve C"
        B --> C[Closed]
    end
    subgraph "Tank"
        C --> D[Sani]
    end
    subgraph "Butterfly Valve B"
      B --> E[Closed]
    end
    subgraph "Drain"
      E --> F[Dry]
    end
</div>

### Position 7

Transfer

<div class="mermaid">
graph LR
    subgraph "Sight Glass"
      AA[Sani]
    end
    subgraph "Butterfly Valve A "
        AA --> A[Open]
    end
    subgraph Tee
        A --> B[Sani]
    end
    subgraph "Butterfly Valve C"
        B --> C[Closed]
    end
    subgraph "Tank"
        C --> D[Sani]
    end
    subgraph "Butterfly Valve B"
      B --> E[Open]
    end
    subgraph "Drain"
      E --> F[Beer/Sani]
    end
</div>
