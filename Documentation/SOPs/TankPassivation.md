# Tank Passivation

**Last Updated: 2019-10-24 by Nick Busey**

## Object and Scope

Tank Passivation is performed regularly to ensure stainless steel tanks stay in good condition.

Every tank must be passivated every three months. Use a dry erase marker to write the date on each tank
so we know when to schedule another passivation.

## Equipment Needed

1. Five Star Acid #5
* Cart Pump
* 2 x Short Cleaning Hoses

## Hazards Identification and PPE / Safety Steps Required:

**PPE:** Eye protection, rubber boots, gloves

**Hazardous Products:** Acid

**Hazards:** Handling hazardous products, pressure.

**Pressure can build when heating a cold tank! A vacuum can occur when cooling a hot tank!**

You should **always** have a tri-clamp open to the tank to let the air equalize and let off any pressure that builds
up with looping a hot sanitizer.

## Preparation

1. [Clean](TankCleaning.md) and rinse the tank
1. Ensure racking arm is at an upward angle so it fills with acid.

## Steps

1. Spray in 15 gallons of hot water from a hose into the tank.
    1. Our hoses spray about 5 gallons per minute. So spray for 3 minutes.
1. Add 15 ounces Five Star Acid #5.
1. Attach hose from pump outlet to CIP inlet.
1. Attach hose from dump port to pump inlet.
1. Run the CIP loop for 30 minutes.
1. Drain, but do not rinse the tank.
1. Open the racking arm and let it drain.
1. Rinse out hoses and pump with clean water.
1. Let the tank air dry for 24 hours.
1. After 24 hours, the tank can be rinsed, sanitized, and used as normal.

## Reference Links

[Passivating Stainless Steel - How To Brew - John Palmer](http://howtobrew.com/book/appendices/appendix-b/passivating-stainless-steel)