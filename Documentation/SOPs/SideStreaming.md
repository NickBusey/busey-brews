# Side Streaming

**Last Updated: 2019-12-09 by Nick Busey**

## Object and Scope

The purpose of this SOP is to ensure compliance with the Town of Nederland's requirements regarding waste.

## Side Streaming

* All high strength waste should be side streamed including spent grains, trub, and excess yeasts.

* pH of cleaning solutions will be measured before discharging to sewer. A pH between 6 and 9 is required for discharge. Cleaning solutions outside this range can be neutralized with baking soda, water dilution or other means to reach a pH in this range.

* If a “bad batch” is made, Busey Brews will contact Nederland Wastewater Treatment Plant prior to dumping the batch down the sewer drains. Town will likely require the batch to be slowly discharged down the drain. 