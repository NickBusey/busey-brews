# Tank Cleaning

**Last Updated: 2021-02-11 by Nick Busey**

## Object and Scope

The purpose of this SOP is to ensure consistency of process relating to the CIP, Sanitization and Purging/Pressurization of a 7 BBL Fermenter.

## Equipment Needed

1. Cart Pump
1. 2 x Short Cleaning Hoses

## Hazards Identification and PPE / Safety Steps Required:

**PPE:** Eye protection, rubber boots, gloves

**Hazardous Products:** Acid, Caustic, Co2

**Hazards:** Handling hazardous products, heat

## Preparation

1. Ensure the tank to be cleaned is empty and rinsed before beginning.
1. Remove all fittings and ports/valves. Anything that can be removed, remove it.
1. After parts are sanitized, re-assemble tank completely.
1. Remove door gasket, clean door and gasket thoroughly.
* Remove fermenter door and gasket.
    * Take out of tank room and clean separately with a brush and caustic.
* Remove carb stone if applicable. Attach it in-line with a T.
* Connect dump port to [transfer tee](TransferTee.md).
* Connect other side of transfer tee to pump inlet.
* Connect pump outlet to CIP inlet.
* Close transfer tee dump side.
1. Ensure any other parts like the tasting port, carb stone, etc. have been cleaned, sanitized, and installed properly before beginning.
1. Attach the cleaned racking arm and turn it so it is pointing up.
    * Open the butterfly valve on the racking arm all the way.
1. Open the sample port, leave it open so sanitizer runs through it.

## Cleaning Fermenters

### CIP
1. Fill tank with 3 minutes of hot water from the hose.
1. Add super CIP to tank. (If it's not a conical bottom, pre-mix powder with hot water outside of tank.)
* Replace fermenter door and gasket.
* Start pump
* Let loop run for 15 minutes.
* Stop pump and dump the liquid.

### Rinse
1. Fill tank with 3 minutes hot water from hose
* Let loop run for 10 minutes.
* Stop pump and dump the liquid.

### Sanitize
1. Add 15 gallons hot water to the tank.
1. Add 3 ounces StarSan to the water.
1. Attach a cleaning hose to the bottom port of the tank and the pump inlet.
1. Attach a cleaning hose to the pump outlet and to the CIP arm inlet.
1. Ensure all butterly valves in the CIP loop are open, and start the pump.
1. Once the loop is running, close the butterly valve on the racking arm to allow it to fill with sanitizer.

## Reference Links

[Baerlic Brewing Co SOP: Brite Tank CIP](https://docs.google.com/document/d/104BeZZ-xZyXBBXoZETXykyxGHBYdOG-0vbc_wi7WzX8/)