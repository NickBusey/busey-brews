# Cleaning Draft Lines

**Last Updated: 2020-01-30 by Nick Busey**

## Object and Scope

To ensure a consistent, reliable process around cleaning our draft lines.

The 'final mile' between the serving tank or keg and the tap is one of the most important transfers in the life of a beer.

Dirty lines are easy to detect just by taste and smell to experienced beer drinkers. We must make every effort to not offend the palates of our customers, and ruin our great beers with an easy to prevent problem.

## Equipment Needed

1. Liquid Line Cleaner
* Line Cleaning Keg

## Hazards Identification and PPE / Safety Steps Required:

**PPE:** Eye protection, rubber boots, gloves

**Hazardous Products:** Acid Cleaner

**Hazards:** Eye, skin, and inhalation hazards! Read all product labels thoroughly before using.

## Preparation

1. Be sure to tell any bartenders or servers who might pour a beer to hold off until you finish.

## Steps

1. Empty and rinse line cleaning keg. Fill with cold water.
2. Add 3.5 oz Liquid Line Cleaner (LLC) to the keg.
3. Close the keg up.
4. Attach the cleaning keg to beer lines
    * For kegs
        * Remove the connection, connect to the cleaner keg. Easy.
    * For serving tanks
        * Disconnect the liquid disconnect on the serving line, attach to a sanke connection, borrow a Co2 line if needed from a keg line.
5. Go upstairs and get an empty pitcher. Put it under the spout of the tap currently hooked up to the cleaning keg and run it until LLC appears (light brown). Stop the tap.
6. Move to the next tap being cleaned and repeat this process.
7. Return to the keg cooler, and remove the lines attached to the cleaning keg, leaving them hanging for rinse.
8. Repeat step 4 through 7 for all lines to be cleaned.
9. Empty and rinse the keg cleaning keg.
10. Fill the keg cleaning keg with cold water.
11. Starting from the first lines that were cleaned, move back through them in the same order. Attach them as before, and run LLC out until clear water appears. Fill a full pitcher of rinse water from each line.
12. Re-connect all beer lines to their original kegs and serving tanks.
13. Empty keg cleaning keg and leave open to dry.

## Reference Links

None yet