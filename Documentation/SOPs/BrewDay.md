# Brew Day

## Valves

First close EVERY valve, including the run off valve.

## Water

First we need our water to be at the right temperature.

Mash strike water temp will drop around 15 deg F when it meets the grain.

If we want to mash at 154 F (a good typical number), we want the water coming out of the mash in arm at 169 F.

Our temp gauge for the kettle (where our sparge water comes from) is 15 degrees under. So that needs to read at least 184F.

The kettle can get a stratified, so before beginning, recirc the kettle water through the Whirlpool arm for at least 5 minutes to mix it. The temperature should reach a fairly steady state once it is all mixed.

## Mash In

Add enough water to the mash for the first bag to be able to fit in the water completely, stirred in with a paddle. Repeat for all grain. Get all the water out of the kettle and into the mash tun if possible. We don't want to lose that heated water, and any we can't take into the tun we have to dump.

## Vorlauf

After the mash has been going for 30 minutes, start a slow vorlauf. Add enzymes now.

FAA 8mL
Bioglucanase 15mL

Vorlauf for 15 minutes then begin runoff.

When using city water to balance temp, be sure to always shut off the city water whenever you shut off the water to the mash in.

## Runoff / Sparging

Open the valve near the runoff port to allow a medium-slow trickle through.

Wait for the water on top of the grain bed to get down within a few inches (around 2) from the bed before beginning to add sparge water.

Sparge water should be around 175F.

Similarly to the kettle above, recirc through the HLT recirc arm for 5 minutes before using HLT water to mix the temp.

## Boil

Once the wort gets to the bottom rung of the ladder, turn on Heat Zone 1. Adjust the target temp to 214. This should be just above the boil point so keep a rolling boil the whole time without getting too hot.



