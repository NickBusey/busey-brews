# SOP Title

**Last Updated: 2019-10-09 by Nick Busey**

## Object and Scope

Define the purpose of the SOP and what will be covered.

## Equipment Needed

1. A list of everything that will be needed to complete this SOP.
* More equipment
* More

## Hazards Identification and PPE / Safety Steps Required:

**PPE:** Eye protection, rubber boots, respirator, gloves

**Hazardous Products:** chemicals, gasses, etc.

**Hazards:** Handling hazardous products (almost always), heat, working at height, LOTO, etc. 

## Preparation

1. Copy the raw contents of this file.
1. Create a new file with the name of the SOP in CamelCase.
1. Paste this file into the new file.
1. Edit as needed.
1. Save and create a git commit and push to GitLab.
1. Use 1. for the start of every numbered step. The software will render the numbers as they should appear.
    1. This allows you to not have to worry about re-numbering when adding new steps.

## Steps

1. More Steps

## Reference Links

* [Writing it Down! SOPs: The Foundation of Any Brewery Quality Program](https://www.brewersassociation.org/seminars/writing-it-down-sops-the-foundation-of-any-brewery-quality-program/)
* [SOP Template](https://docs.google.com/document/d/1CGs0_PrNgRo6sNEkFQLKV4aML2P1DC64LLdagqQAwrk/edit#heading=h.ni8nbliewtnr)
