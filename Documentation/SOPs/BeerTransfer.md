# Beer Transfer

**Last Updated: 2019-10-09 by Nick Busey**

## Object and Scope

To ensure a consistent, reliable transferring process.

## Equipment Needed

1. 2 x Medium (12') Beer Hoses
1. Fining agent (Biofine)

## Hazards Identification and PPE / Safety Steps Required:

**PPE:** Eye protection, rubber boots, gloves

**Hazardous Products:** None

**Hazards:** Pressure! Ensure the receiving tank has a top valve open so gas can escape and not build up.
Ensure the fermenter has an opening to prevent a vacuum from forming.

## Preparation

1. Ensure your receiving tank has been [cleaned and sanitized](TankCleaning.md) before beginning.

## Steps

1. With the bottom valve on the receiving tank closed and the top valve slightly open, turn on the Co2 to the carbonating stone and allow it to run for two minutes. This purges the tank of any oxygen.
1. Add fining agent to receiving tank if applicable. The incoming beer will mix it up.
    1. Make sure to have co2 pushing into the tank while removing the door to add the fining agent. This prevents as much outside air as possible from entering the tank.
1. Add a [transfer tee](TransferTee.md) to the bottom/dump valve on the receiving tank. Open the dump valve on the tee.
1. Connect beer lines from the tank to the top left butterfly valve on the brew house manifold. Use 2 butter fly valves here, so once the lines are full of sani you can close both valves and disconnect the hose between the two valves, keeping one valve on the hose. This keeps the hose packed full of sanitizer.
1. Make sanitizer in the mash tun, 15 gallons water (3 minutes of hose) plus 3 ounces StarSan.
1. Pump the sanitizer through the lines for at least 10 seconds, ensuring they are completely filled with sanitizer. Close the dump valve on the transfer tee. Close both valves between the beer hose and the brew house manifold. Turn off the pump.
1. Remove the tri-clamp between the two valves, and with sanitizer spray bottle in hand, move the hose to the sending tank. Sanitize the inside and outside of the racking arm valve, attach the hose.
1. IMPORTANT: Attach the Co2 line to the co2 port on the blowoff arm of the sending tank. Turn on co2 at low pressure (just 2-3 PSI).
    1. Not doing this can create a vacuum in the tank and crush it or the glycol lining.
1. Open the dump valve on the transfer tee at the receiving tank.
1. Open the valves on the racking arm of the sending tank.
1. Go back quickly to the receiving tank, once all the sanitizer is out of the hose and it's dumping 100% beer, after a 3 second count, close the dump valve and open the tank in valve.
1. Monitor the transfer until the sight-glass has no more movement. Shut both valves.
1. Apply head pressure to the new tank to begin the carbonation process.

## Reference Links

None yet