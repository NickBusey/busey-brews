# Cleaning the Heat Exchanger

The rear set of packs, between the back thick outer wall and the back most plate of the stack, the two o-rings go there.

The other two orings go at the back of the first stack, against the front side of the middle thick wall.

Tighten entire stack including outer thick walls to 7 - 3/16"

Use a ratch strap around the plates to make removal and re-installation easier.

