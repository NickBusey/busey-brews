# Keep a Beginner / Learners Mindset

No one knows everything.

# Don't be a Hero

Identify and work together with co-workers to improve tasks that are hard to perform.
Don't play the 'who works harder' game.

# Always Ask Questions

Better to ask than to not and do something wrong.

# We're on the same team

`That's not my job` shouldn't be a phrase we use. Help out coworkers who need it.