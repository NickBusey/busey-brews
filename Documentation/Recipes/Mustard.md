# Mustard

https://www.youtube.com/watch?v=8ktONdU_wXU Recipe multiplied by 5.

## Ingredients

### Dry

30 Tbsp Brown Mustard Seeds - 375g 
25 Tbsp Yellow Mustard Seeds - 313g
5 Tbsp Yellow Mustard Powder
10 Tbsp Kosher Salt

### Wet

1 1/4 cup apple cider vinegar (used 2.5 cups, too much)
1 1/4 cup verjus - unfermented sour grape juice
  Can replace with dry vermouth, or dry white wine (used 2 cups, too much)
25 Tbsp Saurkraut Juice

## Method

Mix mustard seeds.
Blend.
Add salt and mustard powder.
Mix everything together.
Ferment for 3-4 days.
Refrigerate.
