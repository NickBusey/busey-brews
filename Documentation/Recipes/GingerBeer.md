Ginger Beer:

* 1qt Ginger Juice (1 btl)
* 1qt Lemon Juice
* 3qt Cinnamon Simple Syrup
 * (2:1 sugar (2qt) and water (1qt) simmered with 12 cinnamon sticks, rested for 30min)
* 3gal water 

Strain all ingredients with a cheesecloth or sieve into 5 gallon korny keg. 

Carb for at 24hrs.