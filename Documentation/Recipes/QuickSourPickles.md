# Quick Sour Pickles

This is one of the easiest ferments you can make at home.

We use a 3.5% [salt brine](https://myfermentedfoods.com/tools/brine-calculator/).

## Ingredients

* 20 lbs cucumbers
* 200 g Salt
* 2 gallons water
* 180 g garlic
* 3.5 oz pickling spice
* Grape leaves
* Clean 5 Gallon Bucket
* Fermentation Weight and Cover

## Steps

1. Wash then cut the bloom ends off of the cucumbers. Add to bucket.
    * The blossom end contains an enzyme that will soften the pickles, trimming 1/16″ is all that is needed to get rid of it.
* Add grape leaves to bucket. The tannins keep the pickles crunchy as they ferment.
* Crush and slice garlic to [release allicin](https://youtu.be/vD6XWXj9l8Q?t=125). Add to bucket.
* Add pickling spice to bucket.
* Place lid on bucket firmly. Shake bucket vigorously to coat the spices around evenly.
* Add 200 g salt to 2 gallons of water. Stir to dissolve.
* Remove lid. Add brine.
* Place fermentation cover on top.
* Place fermentation weight or spacer on top of cover.
* Ensure no cucumbers are exposed to the air. The brine and spices can flow above the lid without issue.
* Place lid on bucket firmly.
* Place in fridge.
* Wait 4 days.
* Taste daily until taste is right.
* Slice and serve!




3.5% salt brine






