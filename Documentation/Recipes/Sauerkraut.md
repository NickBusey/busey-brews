# Ingredients

* 5 lbs cabbage
* 50 g salt (10g salt per lb cabbage)
* 2 tbsp mustard seeds
* 2 tbsp Caraway seeds
* 4 Garlic cloves

# Directions

* De-core and chop cabbage.
* Add half of the salt to the cabbage in a large mixing bowl, mix it up.
* Mash the cabbage down with your fists, crushing it up.
* Cover and wait 20 minutes.
* Add the rest of the salt, mix it up.
* Crush with fists.
* Cover and wait 20 minutes.
* Mince the garlic.
* Add garlic, mustard, caraway mix it one final time.
* Pack into a large glass jar using a wooden muddler, use a good amount of force.
* Wait 10 minutes and pack it down again firmly.
* Add a fermentation weight and push it down until the brine completely covers all the vegetable matter.
* Add an airlock to the jar with fresh sanitizer.
* Label and date the jar.
* Let sit 2 weeks at room temperature in the fermentation room.
* Refrigerate. It is now ready to serve.