# Fermented Hot Sauce

We make several varietes of fermented hot sauces.

Here is our base recipe.


## Ingredients

- ~400g Habanero Chilis
- ~250g Piri Piri Chilis (exchange with Cayennes, Birds Eye, or similar)
- A couple of yellow Jalapeño peppers for mild fruity heat
- One large Carrot (~160g)
- One large parsnip (~130g)
- Three garlic cloves
- 5% Brine --> __Per Liter__; 950g chlorine free water, 50g salt without additives (sea salt, rock salt or kosher salt)
- Wide-mouth clean jar to ferment in.

## Steps

### The Prep

- Wash all ingredients to remove dirt and any residues of pesticides
- De-seed all chilis and chop roughly (prevents air pockets from forming in the jar)
- Peel and chop carrots and parsnip roughly
- Peel and crush the garlic cloves
- Add everything to the jar and pack it
- Top up with brine, so that it covers all and everything is submerged. Use a spoon to pack it and get rid of any air pockets.
- Weigh down the vegetables with pickle weighs, or simply a small, sealed plastic bag filled with brine

Fermentation should begin within 1-3 days.

### The Ferment

Let sit in room temp, away from direct sunlight for about two weeks, minimum seven days.

### The Bottling

After the ferment, it is time to mix and bottle the chili sauce.

- Strain the vegetables (keep the brine!)
- Mix thoroughly in a kitchen mixer/blender (don't let it get hot, since it can kill the culture)
- Add brine back to the mix, until it has the desired consistency
- Bottle