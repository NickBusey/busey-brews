Beer and pretzels have a long history. It's thought they both came to America on the Mayflower, and they've never been far apart since. Find yourself in any beer festival, and you'll notice pretzel necklaces to be the designer accessory of choice. While we certainly enjoy the crunch of a hard pretzel with our pints, we favor sampling these soft pretzel rolls made with spent grain and boiled in beer with our brews. Slather with spicy mustard or make a killer booze-inspired sandwich.
WHAT YOU NEED

    3 cups warm water (~110 degrees F)
    2 Tbsp dry active yeast
    5 cups flour
    1 cup of Spent Grain Flour
    2 tablespoon sugar
    4 teaspoon kosher salt, plus more for sprinkling
    10 cups water
    2 cup beer (measuring cup, not a whole beer!)
    1/2 cup baking soda

WHAT YOU DO

1. In a stand mixer add the warm water and sprinkle yeast on top. Let stand five minutes until small bubbles appear.

2. Combine flours, sugar, and salt.

3. Using the dough hook attachment dump in flour mixture and mix on low until the dough comes together. Increase speed to medium and mix until dough is elastic and smooth (about 6 more minutes).

4. Remove dough from mixer, form into a ball and place in oiled mixing bowl, turning once to coat. Let rise 30 minutes covered in a damp dish towel.

5. Turn dough out onto a floured surface and knead for 1 minute. Divide dough into 16 balls. Place on an oiled baking sheet or cupcake tin, score top of each roll, cover with a damp dish towel and let rise a second time (about 20 minutes).

6. Preheat oven to 425 degrees F. Bring water and beer to a boil in a large pot on the stove top. We recommend using at least a two gallon pot as the water tends to boil over with the addition of the baking soda.

7. When rolls have risen, add the baking soda to the boil (it will foam up a bit). Using a slotted spoon boil rolls for 2 minutes per side.

8. Transfer to baking sheet with rolls scored side up. Sprinkle with salt and bake 12 minutes. Enjoy.
