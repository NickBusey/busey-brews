# How to make Kombucha

**Last Updated: 2019-10-24 by Nick Busey**

## Ingredients

* 14 Gallons Water
* 7 Large Iced Tea Bag
* 3150 Grams Sugar
* 16 Cups Starter Fluid
* Scoby that covers at lesat 25% of the surface of the container
* Two clean 7 Gallon Plastic Container
* Clean White Rag to use as Air Lock
* Black O-Ring or Rubber Band to attach White Rag

## Steps 

* Bring 1/2 of the total volume to a near boil
* Stop heat, add sugar, stir to dissolve.
* Add tea bags, steep at least 20 minutes
* Remove bag carefully, so it doesn't tear.
    * If you do tear the bag, strain it through some cheese cloth.
* Cover with lid and wait for it to cool to luke warm temperature.
* Add scoby.
* Pour starter fluid on top of the scoby, slowly and gently, to form a 'seal' of the starter fluid on top of the container.
* Let ferment one week.
* Taste daily until desired acidity is reached.

### Note

Finished booch will have a pH between 3.5 and 4.0 with just a touch of sweetness left.

## Reference Links

[Reddit Master Kombucha Recipe](https://www.reddit.com/r/Kombucha/comments/5b1ztm/reddit_master_kombucha_recipe/)