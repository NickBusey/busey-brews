# Birch Beer

* Add 8 lbs brown sugar to water
* Add 1 lb birch bark to water
* Boil for 10 minutes
* Add yeast
* Ferment for 10 days
* Keg
